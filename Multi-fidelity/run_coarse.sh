#!/bin/sh


echo '> evaluation starts ...'

######### simulation parameters #####

DEGREE=1
NELEM=8

GAUSS=`echo print $DEGREE + 1 | perl`

########## cleaning ##############
rm -rf Eval
mkdir Eval

cd Eval

cp ../design_vector_0.dat solver_parameters.dat

########### run case ##########

~/Devel/igloo/build/bin/igGenMesh -case viscous_burgers -mesh mesh.dat -n1 $NELEM -degree $DEGREE -gauss $GAUSS  > igGenMesh.log
~/Devel/igloo/build/bin/igGenSol -case viscous_burgers -mesh mesh.dat -initial initial.dat -optimize > igGenSol.log
time ~/Devel/igloo/build/bin/igloo -solver viscous_burger -mesh mesh.dat -initial initial.dat -time 1. -integrator rk4 -error -cfl 0.25 -optimize > igloo.log

########### end ##########

cp cost_function.dat ../simulation_result_0.dat

cd ..

echo '> evaluation done'

exit 0
