#!/usr/bin/env python
# coding: utf-8

# # initialization

# In[1]:


import numpy as np
import tensorflow as tf

np.random.seed(1793)
tf.random.set_seed(1793)


# # Problem definition : Branin

# In[2]:


from trieste.objectives import (
    scaled_branin,
    SCALED_BRANIN_MINIMUM,
    BRANIN_SEARCH_SPACE,
)
from trieste.objectives.utils import mk_observer
from trieste.space import Box

search_space = Box([0, 0], [1, 1])


# # Design of Experiment

# In[3]:


import trieste

observer = trieste.objectives.utils.mk_observer(scaled_branin)

num_initial_points = 8
initial_query_points = search_space.sample_sobol(num_initial_points)
initial_data = observer(initial_query_points)


# In[4]:


print(f"DoE points: \n {initial_query_points[:, :]}")
print(f"DoE values: \n {initial_data.observations[:]}")


# In[5]:


from matplotlib import pyplot;

pyplot. clf();
pyplot.scatter(initial_query_points[:,0], initial_query_points[:,1], label="DoE", marker="+");
pyplot.legend();
pyplot.savefig('doe.png')


# # GP model construction

# In[6]:


from trieste.models.gpflow import build_gpr
from trieste.models.gpflow import GaussianProcessRegression

gpflow_model = build_gpr(initial_data, search_space, likelihood_variance=1e-7)
model = GaussianProcessRegression(gpflow_model, num_kernel_samples=100)


# # Optimization

# In[7]:


bo = trieste.bayesian_optimizer.BayesianOptimizer(observer, search_space)

num_steps = 30
result = bo.optimize(num_steps, initial_data, model)
dataset = result.try_get_final_dataset()


# # Post-processing of results

# In[8]:


query_points = dataset.query_points.numpy()
observations = dataset.observations.numpy()

arg_min_idx = tf.squeeze(tf.argmin(observations, axis=0))

print(f"query point: {query_points[arg_min_idx, :]}")
print(f"observation: {observations[arg_min_idx, :]}")


# In[9]:

pyplot. clf();
pyplot.scatter(query_points[:,0], query_points[:,1], label="EGO", marker="o");
pyplot.scatter(initial_query_points[:,0], initial_query_points[:,1], label="DoE", marker="+");
pyplot.legend();
pyplot.savefig('observations.png')


# In[13]:

pyplot. clf();
pyplot.plot(observations[:], label="observations", marker="+");
pyplot.xlabel("evaluation number");
pyplot.ylabel("cost function");
pyplot.legend();
pyplot.savefig('cost.png')


# In[ ]:
