# Submission script for the helloWorld program
#
# Comments starting with #OAR are used by the 
# resource manager
#
#OAR -l /nodes=1/core=16,walltime=48:00:00
#OAR -p cluster='dellc6420'
#
#OAR -q default
# The job is submitted to the default queue

module load mpi/openmpi-1.10.7-gcc
module load conda/2021.11-python3.9


conda init bash
conda activate trieste_conda

python ./test_ego.py

conda deactivate






