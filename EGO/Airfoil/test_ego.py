#!/usr/bin/env python
# coding: utf-8

##### main parameters ###########

dim = 16

num_initial_points = 32
num_steps = 68

const_threshold = 0.


#################################

# ## Initialization of random Generator

import numpy as np
import tensorflow as tf

np.random.seed(1976)
tf.random.set_seed(1976)

# ## Optimization settings
# We start by defining the search space.

import trieste
from trieste.space import Box

search_space = trieste.space.Box(
    [0.03, 0.04, 0.04, 0.04, 0.04, 0.03, 0.02, 0.01, -0.03, -0.05, -0.06, -0.07, -0.07, -0.05, -0.03, -0.02],
    [0.04, 0.07, 0.08, 0.08, 0.08, 0.07, 0.05, 0.04, -0.02, -0.03, -0.04, -0.04, -0.04, -0.02, -0.00, -0.00]
)

# We define now the Trieste observer to get cost function and constraint values from a set of design vectors. 

import subprocess
from trieste.data import Dataset

OBJECTIVE = "OBJECTIVE"
CONSTRAINT = "CONSTRAINT"

def observer(query_points):
    
    x = query_points[:]
    size = x.get_shape()[0]
    sim_values = np.zeros(size)
    const_values = np.zeros(size)
    
    print(f"x: {size}")
    print(f"x: \n {query_points[:]}")

    for i in range(size):

        f_out = open("design_vector_0.dat", "w")
        f_out.write(str(dim))
        f_out.write("\n")
        np.savetxt(f_out, x[i])
        f_out.close()

        subprocess.run(["./run.sh"])

        f_in_sim = open("simulation_result_0.dat", "r")
        line1 = f_in_sim.readline()
        line2 = f_in_sim.readline()
        line3 = f_in_sim.readline()
        sim_number = int(line1)
        sim_flag = int(line2)
        sim_values[i] = float(line3)
        
        f_in_const = open("simulation_result_1.dat", "r")
        line1 = f_in_const.readline()
        line2 = f_in_const.readline()
        line3 = f_in_const.readline()
        const_number = int(line1)
        const_flag = int(line2)
        const_values[i] = float(line3)

    print(f" value: \n {sim_values[:]}")
    print(f" value: \n {const_values[:]}")

    return {
        OBJECTIVE: Dataset(query_points, sim_values[:, None]),
        CONSTRAINT: Dataset(query_points, const_values[:, None]),
    }


# Then, we define a Design of Experiment based on random distribution.

initial_query_points = search_space.sample(num_initial_points)
print(f"DoE points: \n {initial_query_points[:, :]}")


# We compute the observation values using the simulator.

initial_data = observer(initial_query_points)


# ## Bayesian optimization
# We start by constructing the GP model based on the Design of Experiment observations.

import trieste
from trieste.models.gpflow import build_gpr, GaussianProcessRegression


def create_bo_model(data):
    gpr = build_gpr(data, search_space, likelihood_variance=1e-7)
    return GaussianProcessRegression(gpr)


initial_models = trieste.utils.map_values(create_bo_model, initial_data)


# We define now the acquisition function used to drive the Bayesian optimization. Here we use the Expected Improvement criterion associated to the Probability of Feasibility criterion for the constraint. Note that the constraint threshold is used as argument

from trieste.acquisition.rule import EfficientGlobalOptimization

pof = trieste.acquisition.ProbabilityOfFeasibility(threshold=const_threshold)
eci = trieste.acquisition.ExpectedConstrainedImprovement(
    OBJECTIVE, pof.using(CONSTRAINT)
)
rule = EfficientGlobalOptimization(eci)


# Now we cam solve the optimization problem.

bo = trieste.bayesian_optimizer.BayesianOptimizer(observer, search_space)

data = bo.optimize(
    num_steps, initial_data, initial_models, rule, track_state=False
).try_get_final_datasets()


# ## Analysis of the results
# First we plot the evolution of the cost function and constraint values.

from matplotlib import pyplot

pyplot.clf()
pyplot.plot(data[OBJECTIVE].observations[:], label="function", marker="+")
pyplot.plot(data[CONSTRAINT].observations[:], label="constraint", marker="o")
pyplot.xlabel("evaluation number")
pyplot.ylabel("cost function")
pyplot.legend()
pyplot.savefig('cost.png')

# Second we plot the evolution of the three design variables.

pyplot.clf()
pyplot.plot(data[OBJECTIVE].query_points[:,0], label="x0", marker="+")
pyplot.plot(data[OBJECTIVE].query_points[:,1], label="x1", marker="o")
pyplot.plot(data[OBJECTIVE].query_points[:,2], label="x2", marker="*")
pyplot.xlabel("evaluation number")
pyplot.ylabel("design variables")
pyplot.legend()
pyplot.savefig('variables.png')

# Finally, we get the best point.

query_points = data[OBJECTIVE].query_points.numpy()
observations = data[OBJECTIVE].observations.numpy()

arg_min_idx = tf.squeeze(tf.argmin(observations, axis=0))

print(f"query point: {query_points[arg_min_idx, :]}")
print(f"observation: {observations[arg_min_idx, :]}")





